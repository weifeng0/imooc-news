'use strict';
const db = uniCloud.database()
//event为客户端上传的参数
exports.main = async (event, context) => {
	const collection = db.collection('test');
	//增加
	// async异步接口  await同步接口
	// let res =await collection.add(
	// 	[{
	// 			name: 'uni-app'
	// 		},
	// 		{
	// 			name: 'html',
	// 			type: '前端'
	// 		}
	// 	]
	// )
	
	//删除
	// let res=await collection.doc("5eb10940ad7441004c06753d").remove();
	
	//update更新,记录不存在的时候，更新失败
	// let res=await collection.doc("5eb1097115bcf2004dce8fa9").update({
	// 	name:"lllllllll"
	// });
	//set更新,记录不存在的时候，自动增加
	// let res=await collection.doc("123").set({
	// 	name:"lllllllll",
	// 	type:"后端",
	// 	author:"李涛"
	// });
	
	//查询1 by ID
	// let res=await collection.doc("5eb1097115bcf2004dce8fa9").get();
	let res=await collection.where({
		name:event.name
	}).get();
	
	//返回数据给客户端
	return {		
		code:200,
		msg:"查询成功",
		name:event.name,
		data:res.data
	}
};
